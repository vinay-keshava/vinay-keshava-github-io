---
layout: page
title: Vinay Keshava
meta-title: About me
---

<div id="aboutme-section">
<p class="about-text">


<h3>echo “Hello World !” <br></h3><br>
Hi I’m Vinay , currently a computer science undergraduate student.<br>
Now writing blog posts in my free time.<br>
I go by the handle @vinay-keshava all over the internet.<br>
Welcome to my  page ,here i try to blog my experiences of learning and experimenting things over time.<br>
<br><br>
I love Photography ,<a href="https://vinay-keshava.gitlab.io/travel">Here</a> are some of  my captures.<br>
I also write blog posts in my free time, you can check my blog posts <a href="https://vinay-keshava.gitlab.io/blog">Here</a>.
<br>
<br>

Email- vinaykeshava AT disroot.org
Matrix-  <a href="https://matrix.to/#/@vinay-keshava:mozilla.org">@vinay-keshava:mozilla.org</a> <br>
XMPP-  vinaykeshava@disroot.org<br>
Mastodon- <a href="https://mastodon.technology/@vinaykeshava">@vinaykeshava</a>

<br>
Thank you for visiting.

