---
layout: page
title: Travel 
---


<span style="display: inline-block; width:300px; line-height: 35px; color:white; font-family: Calibri; background-color:#37751cff; font-size: 105%;border-radius: 8px; text-align:center;"><b>Karnataka,28-July-2020</b></span>
<br><br>
<img src="/img/28_jul_20.jpg" width=400 height=600>
<br>
<span style="display: inline-block; width:300px; line-height: 35px; color:white; font-family: Calibri; background-color:#37751cff; font-size: 105%;border-radius: 8px; text-align:center;"><b>Karnataka,25-July-2020</b></span>
<br><br>
<img src="/img/25_jul_20.jpg">
<br>

<span style="display: inline-block; width:300px; line-height: 35px; color:white; font-family: Calibri; background-color:#37751cff; font-size: 105%;border-radius: 8px; text-align:center;"><b>04-Jun-2020</b></span>
<br><br>
<img src="/img/4_jun_2020.jpg" width=400 height=600>
<br><br>
<span style="display: inline-block; width:300px; line-height: 35px; color:white; font-family: Calibri; background-color:#37751cff; font-size: 105%;border-radius: 8px; text-align:center;"><b>Moodbidri, Karnataka,29-Feb-2020</b></span>
<br><br>
<img src="/img/29_feb_20.jpg" width=400 height=600>
<br><br>

<span style="display: inline-block; width:300px; line-height: 35px; color:white; font-family: Calibri; background-color:#37751cff; font-size: 105%;border-radius: 8px; text-align:center;"><b>Macro Photography</b></span>
<br><br>
<img src="/img/eye.jpg" width=400 height=500>
<br><br>


<span style="display: inline-block; width:300px; line-height: 35px; color:white; font-family: Calibri; background-color:#37751cff; font-size: 105%;border-radius: 8px; text-align:center;"><b>19-Apr-2020</b></span>
<br><br>
<img src="/img/spark.jpg" >
<br><br>

<span style="display: inline-block; width:300px; line-height: 35px; color:white; font-family: Calibri; background-color:#37751cff; font-size: 105%;border-radius: 8px; text-align:center;"><b>Andhra Pradesh,2-Feb-2020</b></span>
<br>
<br>
<img src="/img/2_feb_20.jpg" width=400 height=600>
<br>
<br>
<span style="display: inline-block; width:300px; line-height: 35px; color:white; font-family: Calibri; background-color:#37751cff; font-size: 105%;border-radius: 8px; text-align:center;"><b>Udupi Karnataka,01-November-2019</b></span>
<br>
<br>
<img src="/img/1_nov_19.jpg" width=400 height=600>
